/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package colas;

import listasimple.Nodo;

/**
 *
 * @author enrique
 */
public class ColaTest {
    public static void main(String[] args) {
        Cola cola = new Cola();
        cola.encolar(new Nodo(10));
        cola.encolar(new Nodo(20));
        cola.encolar(new Nodo(30));
        
        Nodo del1 = cola.desencolar();
        cola.desencolar();
        cola.desencolar();
        
        System.out.println("El nodo eliminado es: " + del1);
        System.out.println("El nodo al frente es: " + cola.getFrente());
    }
}
