/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package colas;

import listasimple.*;

/**
 *
 * @author enrique
 */
public class Cola {
    private Lista lista;
    
    public Cola() {
        lista = new Lista();
    }
    
    public boolean colaVacia() {
        return lista.estaVacia();
    }
    
    public void encolar(Nodo n) {
        lista.insertarUltimo(n);
    }
    
    public Nodo desencolar() {
        return lista.eliminarPrimero();
    }
    
    public Nodo getFrente() {
        return lista.getPrimero();
    }
}
