/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package listasimple;

/**
 *
 * @author Jose Enrique Avila <jeavila@unah.edu.hn>
 */
public class Nodo {
    private int dato;
    private Nodo siguiente;
    
    public Nodo() {
        this.dato = 0;
        this.siguiente = null;
    }
    
    public Nodo(int dato) {
        this.dato = dato;
        this.siguiente = null;
    }
    
    public Nodo(int dato, Nodo n) {
        this.dato = dato;
        this.siguiente = n;
    }
    
    public Nodo getSiguiente() {
        return this.siguiente;
    }
    
    public void setSiguiente(Nodo n) {
        this.siguiente = n;
    }
    
    public int getDato() {
        return this.dato;
    }
    
    @Override
    public String toString() {
        return String.format("Dato: %s", this.dato);
    }
}
