/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package listasimple;

/**
 *
 * @author Jose Enrique Avila <jeavila@unah.edu.hn>
 */
public class Lista {

    private Nodo primero;
    private Nodo ultimo;

    /**
     * @return the primero
     */
    public Nodo getPrimero() {
        return primero;
    }

    /**
     * @param primero the primero to set
     */
    public void setPrimero(Nodo primero) {
        this.primero = primero;
    }

    /**
     * @return the ultimo
     */
    public Nodo getUltimo() {
        return ultimo;
    }

    /**
     * @param ultimo the ultimo to set
     */
    public void setUltimo(Nodo ultimo) {
        this.ultimo = ultimo;
    }

    public Lista() {
        vaciar();
    }

    public void vaciar() {
        setPrimero(null);
        setUltimo(null);
    }

    public boolean estaVacia() {
        return getPrimero() == null;
    }

    public void recorrer() {
        int contador;
        Nodo item;

        if (estaVacia()) {
            System.out.println("Lista vacia!");
        } else {
            System.out.println("Primero -> " + getPrimero());
            System.out.println("Ultimo -> " + getUltimo());
            
            contador = 0;
            item = getPrimero();

            do {
                System.out.printf("Nodo%d -> %s %n", contador, item);
                item = item.getSiguiente();
                contador++;
            } while (item != null);
        }
    }

    public void insertarPrimero(Nodo n) {
        if (estaVacia()) {
            setUltimo(n);
        } else {
            n.setSiguiente(getPrimero());
        }

        setPrimero(n);
    }

    public void insertarUltimo(Nodo n) {
        if (estaVacia()) {
            insertarPrimero(n);
        } else {
            getUltimo().setSiguiente(n);
            setUltimo(n);
        }
    }

    public void insertarEnmedio(Nodo n, Nodo nAnterior) {
        if (nAnterior == null) {
            return;
        }
        
        if (estaVacia()) {
            insertarPrimero(n);
        } else {
            if (getUltimo() == nAnterior) {
                setUltimo(n);
            }
            
            n.setSiguiente(nAnterior.getSiguiente());            
            nAnterior.setSiguiente(n);
        }
    }

    public Nodo eliminarPrimero() {
        Nodo eliminado = null;        
        
        if (!estaVacia()) {            
            eliminado = getPrimero();

            if (getPrimero() == getUltimo()) {
                vaciar();
            } else {
                setPrimero(getPrimero().getSiguiente());
                eliminado.setSiguiente(null);
            }
        }

        return eliminado;
    }

    public Nodo eliminarUltimo() {
        Nodo eliminado = null;
        
        if (!estaVacia()) {
        
            eliminado = getUltimo();

            if (getPrimero() == getUltimo()) {
                vaciar();
            } else {
                Nodo item = getPrimero();

                while(item.getSiguiente() != getUltimo()) {
                    item = item.getSiguiente();
                }

                setUltimo(item);
                item.setSiguiente(null);
            }
        }

        return eliminado;
    }
        
    
    public Nodo eliminarEnmedio(Nodo n) {
        Nodo eliminado = null;
        
        if (n != null && !estaVacia()) {
            eliminado = n;
        
            if (n == getPrimero()) {
                eliminarPrimero();
            } else if (n == getUltimo()) {
                eliminarUltimo();
            } else {
                Nodo item = getPrimero();

                while (item.getSiguiente() != n) {
                    item = item.getSiguiente();
                } 

                item.setSiguiente(item.getSiguiente().getSiguiente());
                n.setSiguiente(null);
            }
        }
        
        return eliminado;
    }
    
    public Nodo encontrarPorDato(int dato) {
        Nodo hallado = null;
        
        if (!estaVacia()) {
            Nodo item = getPrimero();

            while (item != null) {
                // Una vez hallado el Nodo debe romperse el bucle
                if (item.getDato() == dato) {
                    hallado = item;
                    break;
                }

                item = item.getSiguiente();
            }
        }
        
        return hallado;
    }
    
    public Nodo encontrarPorIndice(int indice) {
        Nodo hallado = null;
        
        if (!estaVacia()) {
            Nodo item = getPrimero();
            int conteo = 0;

            while (item != getUltimo()) {
                // Una vez hallado el Nodo debe romperse el bucle
                if (conteo == indice) {
                    hallado = item;
                    break;
                }

                conteo++;
                item = item.getSiguiente();
            }
        }
        
        return hallado;
    }    
}
